package co.simplon.springorm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.springorm.entity.Comment;

@Repository
public interface CommentRepo extends JpaRepository<Comment, Integer> {

}
