package co.simplon.springorm.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.springorm.entity.User;

public interface UserRepo extends JpaRepository <User, Integer>{
    Optional<User> findByEmail(String email);
}
