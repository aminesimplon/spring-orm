package co.simplon.springorm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.springorm.entity.Picture;


@Repository
public interface PictureRepo extends JpaRepository<Picture, Integer> {
    
}

