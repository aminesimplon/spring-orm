package co.simplon.springorm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.springorm.entity.Picture;
import co.simplon.springorm.entity.User;
import co.simplon.springorm.repository.PictureRepo;
import co.simplon.springorm.repository.UserRepo;
import jakarta.validation.Valid;


@RestController
public class AuthController {

    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepo repo;

    @Autowired
    private PictureRepo picRepo;



    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@Valid @RequestBody User user) {
        if(repo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }

        String hash = hasher.encode(user.getPassword());
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        repo.save(user);
        return user;
    }

    @GetMapping("/api/account")
    public User myAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @GetMapping("/api/account/picture")
    public List<Picture> myPictures(@AuthenticationPrincipal User user) {
        Picture picture = new Picture();
        picture.setAuthor(user);
        return picRepo.findAll(Example.of(picture));
    }



}
