package co.simplon.springorm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import co.simplon.springorm.entity.Comment;
import co.simplon.springorm.repository.CommentRepo;

public class CommentController {
    @Autowired
    private CommentRepo repo;

    @GetMapping
    public List<Comment> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Comment findById(@PathVariable Integer id) {
        return repo.findById(id).get();
    }

    @PostMapping()
    public Comment postComment( @RequestBody Comment Comment) {
        return repo.save(Comment);
    }
    
    @PostMapping()
    public Comment updateComment( @RequestBody Comment Comment) {
        Comment newPic = findById(Comment.getId());
        return repo.save(newPic);
    }
    
    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable Integer id){
        Comment pic = findById(id);
        repo.delete(pic);
    }

}
