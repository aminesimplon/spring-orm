package co.simplon.springorm.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.springorm.entity.Picture;
import co.simplon.springorm.entity.User;
import co.simplon.springorm.repository.PictureRepo;
import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/api/picture")
public class PictureController {

    @Value("${location}")


    @Autowired
    private PictureRepo repo;

    @GetMapping
    public List<Picture> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Picture one(@PathVariable int id) {
        return repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    
    public Picture add(@AuthenticationPrincipal User user, @Valid @RequestBody Picture picture) {
        picture.setCreatedAt(LocalDateTime.now());
        picture.setAuthor(user);
        repo.save(picture);
        return picture;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Picture pic = one(id);
        if(!pic.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your picture");
        }
        repo.delete(pic);
    }

    @PutMapping("/{id}")
    public Picture update(@PathVariable int id, @Valid @RequestBody Picture picture, @AuthenticationPrincipal User user) {
        Picture toUpdate = one(id);
        if(!toUpdate.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your picture");
        }
        toUpdate.setImage(picture.getImage());
        toUpdate.setDescription(picture.getDescription());
        toUpdate.setTitle(picture.getTitle());
        repo.save(toUpdate);
        return toUpdate;
    }


    @PostMapping("/upload")
    public String postMethodName(@RequestParam MultipartFile image) {       
        String renamed = UUID.randomUUID()+".jpg";
        try {
            Thumbnails.of(image.getInputStream())
            .width(900)
            .toFile("/home/amine/Dev/uploads/thumbnal-" + renamed);
            Thumbnails.of(image.getInputStream())
            .size(200, 200)
            .crop(Positions.CENTER)
            .toFile("/home/amine/Dev/uploads/thumbnal-" + renamed);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Uploading image failed");
        }
        return renamed;
    }
    



}